---
title: Anunciando el proyecto Magma
date: '2018-12-18'
image: "img/bg.jpg"
author: "Vasilis Ververis"
tags: "magma"
---

En años recientes, un cada vez mayor número de periodistas,
activistas de derechos humanos, abogados, así como la comunidad
de investigación en general, están trabajando en contextos de alto
riesgo, los cuales crean la necesidad de considerar sus datos
cualitativos y cuantitativos de investigación como altamente
sensibles. A pesar de su competitividad y altas cualificaciones en
sus respectivas áreas de especialización (ciencias sociales y
políticas, usabilidad, ley, análisis de economía política),
pocas veces se trata de gente que ha tenido experiencia
específica o a profundidad en lo relativo a servicios y sistemas
de red, infraestructura de telecomunicaciones, análisis aplicado
a datos de medidas de red, censura de Internet, vigilancia, y
controles sobre la información.

Idealmente, los investigadores que trabajan con diversas
herramientas de medición y marcos de análisis como el Observatorio
Abierto de Interferencias en la Red (OONI, por sus siglas en
inglés) deberían recibir ayuda y asistencia técnica calificada,
permitiéndoles desarrollar metodologías adeucadas para realizar
las pruebas de una manera acorde para su ambiente y necesidades
de investigación.

Magma busca construir un marco de trabajo para la investigación
para las personas que estudian los controles sobre la información
y mediciones de redes, facilitando el avance en su proceso de
múltiples maneras. Como tal, este marco les permitirá estructurar
un plan de forma adecuada, para realizar elecciones informadas
respecto a las herramientas necesarias (incluyendo los aspectos
éticos y de seguridad), así como analizar los datos producidos
por dichas herramientas.

Nos gustaría proveer nuestra experiencia y expertez en la
realización de mediciones de la red, investigación en censura de
Internet, evaluación de la red de proveedores de servicios de
Internet (ISPs, por sus siglas en inglés), búsqueda de vigilncia,
y análisis de datos para:

    Evaluar los riesgos proveyendo, implementando y manteniendo
    las tecnologías que requieren los investigadores en las áreas
    de frontera y en áreas donde la necesidad de seguridad
    operacional, medidas anti-vigilancia, y resistencia a la
    censura son de primordial importancia;

    Proveer asistencia técnica a medida, desarrollando al mismo
    tiempo metodologías de pruebas adecuadas para la medición de
    redes, evaluación y análisis de datos y reportes que
    correspondan a las preguntas de investigación respectivas;

    Y a largo plazo, construir una metodología escalable y
    reproducible para recolectar, evaluar y analizar datos y
    reportes [que permitan realizar una] autodefensa para los
    investigadores de frontera, para quienes trabajan en el campo,
    defensores de derechos humanos, organizaciones y periodistas,
    manteniendo una documentación exacta.

## Investigación previa

Algunos ejemplos de investigación a futuro alrededor de la censura
en red, controles sobre la información y vigilancia, principalmente
construida realizando mediciones sobre la red y analziando sus
resultados:

- Egipto: [Censura de medios, interferencia con Tor, limitación de HTTPS,
  inyección de
  anuncios](https://ooni.torproject.org/post/egypt-network-interference/)

  *Un estudio sobre la red Tor y sobre bloqueos a sitios de medios,
  limitación de ancho de banda en la red, e inyección de paquetes
  de red maliciosos conteniendo malware y contenido de anunciantes.*

- Los datos de OONI [revelan cómo fue bloqueado (de nueva cuenta) Whatsapp en
  Brasil](https://ooni.torproject.org/post/brazil-whatsapp-block/)

  *Un estudio que determinó cómo se bloqueó Whatsapp edspués de la
  órden de un juez, en todo el país brasileño.*

- [Entendiendo las políticas de censura de Internet: El caso de
  Grecia](https://www.usenix.org/conference/foci15/workshop-program/presentation/ververis)

  *Una investigación extensa y a gran escala analizando las políticas
  y técnicas utilizadas para bloquear contenido determinado ilegal
  por un Estado con problemas de transparencia identificados, daño
  colateral de éstas, y las implicaciones de la sobre- y sub-censura.*

- [Identificando casos de errores de configuración de DNS: No es exactamente
  censura](https://ooni.torproject.org/post/not-quite-network-censorship/)

  *Un estudio de un problema técnico (no malicioso) que lleva a la
  interferencia y no-accesibilidad de una fuente de datos
  noticiosos regionales a través de diversas redes y países.*

Por tal motivo, invito a todos quienes quieran desarrollar investigación
sobre el control sobre la información y la censura de Internet, y que
quieran comprender mejor cómo trabajar con las mediciones de red y
analizar datos de varias fuentes de datos y reportes OONI. Queremos
mantener este mensaje tan congreto y terso como sea posible, y esperamos
que tanto entidades como individuos, con enfoques tanto técnicos como
no-técnicos, se pongan en contacto con nosotros, incluso si en este
momento están involucrados en un proyecto en desarrollo. Los resultados
de esta colaboración ayudarán a la creación de un manual de lineamientos
expresando las necesidades de las comunidades que trabajan o llevan a
cabo tareas de investigación en este campo.

Por favor utilice alguno canale de comunicación (listado en la
[contact página](/contact/)) para entrar en contacto con nosotros.
