---
title: Announcing magma project
date: '2018-12-18'
image: "img/bg.jpg"
author: "Vasilis Ververis"
tags: "magma"
---

Magma aims to build a scalable, reproducible, standard methodology on measuring,
documenting and circumventing internet censorship, information controls,
internet blackouts and surveillance in a way that will be streamlined and used
in practice by researchers, front-line activists, field-workers, human rights
defenders, organizations and journalists.

In recent years, a number of research fellows, journalists, human rights
activists, lawyers as well as a larger activist community, have been working in
high-risk contexts, which create the need to consider their qualitative and
quantitative research data as highly sensitive. Albeit their competitiveness and
high qualification in their respective areas (social and political science,
usability, law, political economy analysis), they can rarely claim to have a
specific expertise or extensive experience regarding networks services and
systems, telecommunication infrastructure, applied data analysis of network
measurements, internet censorship, surveillance and information controls.

Ideally, researchers working with various network measurement tools and
frameworks such as the Open Observatory of Network Interference (OONI), should
have qualified technical help and assistance, thus enabling them to develop
appropriate testing methodologies, suiting exactly their research environment
and needs.

Magma aims to build a research framework for people working on information
controls and network measurements, facilitating their working process in
numerous ways. As such, this framework will enable them to properly structure an
activity plan, make informed choices regarding the required tools (including
numerous ways.

Through Magma, we wish to provide our expertise and experience in network
measurements, internet censorship research, assessment of ISP network,
surveillance probing and data analysis in order to:

    Asses the risks by providing, implementing and maintaining technologies
    demanded by researchers on front-lines and areas where the need of
    operational security, anti-surveillance and censorship circumvention is of
    paramount importance.

    Provide tailored technical assistance, developing at the same time
    appropriate testing methodology for network measurements, evaluation and
    analysis of data and reports that correspond to the respective research
    questions.

    On a long-term basis, build a scalable and reproducible methodology for
    collecting, evaluating and analyzing data and reports’ self-defense for
    front-line researchers, front-line activists, field-workers, human rights
    defenders, organizations and journalists, by keeping exact documentation.

## Previous research

Below, we list some examples of potential future research around internet
censorship, information controls and surveillance, mainly based on conducting
networks measurements and analyzing its results:

- Egypt: [Media censorship, Tor interference, HTTPS throttling and ads
  injections](https://ooni.torproject.org/post/egypt-network-interference/)

  *A study on Tor network and media websites blockages, network bandwidth
  throttling and malicious network packet injections that contained malware and
  advertising content.*

- OONI Data Reveals How [WhatsApp Was Blocked (Again) in
  Brazil](https://ooni.torproject.org/post/brazil-whatsapp-block/)

  *A study to determine how WhatsApp has been blocked after a judge's court
  order all over the country of Brazil.*

- [Understanding Internet Censorship Policy: The Case of
  Greece](https://www.usenix.org/conference/foci15/workshop-program/presentation/ververis)

  *An extensive large scale research analyzing the policies and techniques
  used to block content deemed as illegal by a state identifying
  transparency problems, collateral damage and the implications of over or
  under blocking.*

- [Identifying cases of DNS misconfiguration: Not quite
  censorship](https://ooni.torproject.org/post/not-quite-network-censorship/)

  *A study on a (non malicious) technical issue that leads to the
  interference and non accessibility of a regional news media outlet
  throughout several different network and countries.*

To this respect, we would like to hear from all of you who are interested in
researching information controls and internet censorship, and are intrigued to
better understand how to work with network measurements and analyze data from
various data sources and OONI reports.

We wanted to keep this post as concrete and terse as possible to encourage both
technical and non-technical entities and individuals to get in touch with us,
even if they are currently engaged in an undergoing project. The results of this
collaboration will help form a complete guideline handbook expressed by the
needs of the communities that work, or conduct research, in this field.

Please use any communications channel (listed in [contact page](/contact/)) to
get in touch with us.
