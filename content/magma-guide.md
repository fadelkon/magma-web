---
title: "Magma guide"
image: "img/magma-logo.svg"
---

**What is the magma guide?**

An open-licensed, collaborative repository that provides the first publicly
available research framework for people working to measure information controls
and online censorship activities. In it, users can find the resources they need
to perform their research more effectively and efficiently.

It is available under the following website:
https://magma.lavafeld.org
